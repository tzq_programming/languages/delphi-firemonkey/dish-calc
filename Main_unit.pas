unit Main_unit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.TreeView, System.Rtti, FMX.Grid.Style, FMX.Controls.Presentation,
  FMX.ScrollBox, FMX.Grid, FMX.StdCtrls, FMX.ListBox, FMX.Edit, FMX.SearchBox,

  First_settings_unit, dish_editor_unit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, FMX.Objects;

type
  TMain_Form = class(TForm)
    StyleBook: TStyleBook;
    Layout_menu: TLayout;
    Layout_dish_composition: TLayout;
    Layout_dish_map: TLayout;
    dish_map: TStringGrid;
    Layout_catigories_buttons: TLayout;
    Btn_category_add: TSpeedButton;
    Btn_category_edit: TSpeedButton;
    Btn_category_del: TSpeedButton;
    Layout_dish_map_buttons: TLayout;
    Layout_dish_map_admin_buttons: TLayout;
    Btn_dish_map_add: TSpeedButton;
    Btn_dish_map_edit: TSpeedButton;
    Btn_dish_map_del: TSpeedButton;
    Text_dish_list: TLabel;
    Text_dish_composition: TLabel;
    Btn_print_menu: TSpeedButton;
    dish_map_col_name: TStringColumn;
    dish_map_col_number: TIntegerColumn;
    dish_map_col_energy: TFloatColumn;
    dish_map_col_proteins: TFloatColumn;
    dish_map_col_lipids: TFloatColumn;
    dish_map_col_carbs: TFloatColumn;
    ListBox_categories: TListBox;
    SearchBox_categories: TSearchBox;
    Layout_categories_admin_buttons: TLayout;
    dish_map_col_price: TFloatColumn;
    FDConnection: TFDConnection;
    FD_dish_categories: TFDTable;
    FD_dishes: TFDTable;
    dish_cats_to_dishes: TDataSource;
    temp_text: TLabel;
    FD_dishes_ALL: TFDTable;
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBox_categoriesChange(Sender: TObject);
    procedure Btn_category_addClick(Sender: TObject);
  private
    { Private declarations }
    procedure dish_map_column_resize();
    procedure test_info_to_dish_map();
    procedure generate_dish_list();

    procedure show_temp_text(text:string);
  public
    { Public declarations }
  end;

const LAYOUT_MENU_WIDTH = 250;
const VSCROLLBAR_WIDTH = 25;

//dish_map columns sizes
const COL_NUMBER_WIDTH = 30;
const COL_NAME_MIN_WIDTH = 200;
const COL_ADVANCE_COLUMNS_MIN_WIDTH = 40;


//dish_category sizes
const CAT_HEIGHT = 32;
const CAT_FONT_SIZE = 16;

//dish_categories field names
const CATEGORY_NAME = 'cat_name';

//dishes field names
const DISH_NAME = 'dish_name';
const DISH_ID = 'dish_id';

var
  Main_Form: TMain_Form;

implementation

{$R *.fmx}
{ TMain_Form }

procedure TMain_Form.Btn_category_addClick(Sender: TObject);
begin
  Dish_Editor_Form.ShowModal;
end;

procedure TMain_Form.dish_map_column_resize;
//  ����� ��� ��������� ������� �����������,
//  � ����������� �� ������� �����
begin
  //���� ���� (������ ����)
  Layout_menu.Width := 0.20*ClientWidth;
  if Layout_menu.Width < LAYOUT_MENU_WIDTH then
    Layout_menu.Width := LAYOUT_MENU_WIDTH;

  //����� �����
  var float_columns := [dish_map_col_energy,
                        dish_map_col_proteins,
                        dish_map_col_lipids,
                        dish_map_col_carbs,
                        dish_map_col_price
  ];
  dish_map_col_number.Width := COL_NUMBER_WIDTH;
  var wid := dish_map.Width - VSCROLLBAR_WIDTH - COL_NUMBER_WIDTH;

  dish_map_col_name.Width := 0.40 * wid;
////�������� ���� ����� ����� ������ ������� ������
//  if dish_map_col_name.Width < COL_NAME_MIN_WIDTH then
//    dish_map_col_name.Width := COL_NAME_MIN_WIDTH;

  for var i := 0 to Length(float_columns) - 1 do begin
    float_columns[i].Width := 0.12 * wid;
//  //�������� ���� ����� ����� ������ ������� ������
//    if float_columns[i].Width < COL_ADVANCE_COLUMNS_MIN_WIDTH then
//      float_columns[i].Width := COL_ADVANCE_COLUMNS_MIN_WIDTH;
  end;
end;

procedure TMain_Form.FormResize(Sender: TObject);
begin
  dish_map_column_resize();
end;

procedure TMain_Form.FormShow(Sender: TObject);
begin
  //Form_first_settings.ShowModal;
  //test_info_to_dish_map();
  FDConnection.Connected := True;
  generate_dish_list();
end;

procedure TMain_Form.generate_dish_list;
begin
  BeginUpdate;

  // inside have freeAndNil:
  ListBox_categories.Clear;

  FD_dish_categories.Open();
  FD_dish_categories.First;

  for var i := 0 to FD_dish_categories.RecordCount - 1 do begin
    // category create
    var ListboxGroup := TListBoxGroupHeader.Create(Self);
      with ListboxGroup do begin
        Height := CAT_HEIGHT;
        StyledSettings := [TStyledSetting.Family, TStyledSetting.FontColor, TStyledSetting.Other];
        TextSettings.Font.Size := CAT_FONT_SIZE;
        TextSettings.Font.Style := [TFontStyle.fsBold];
        Text := FD_dish_categories.FieldByName(CATEGORY_NAME).AsString;
//        Tag := FD_dish_categories.FieldByName('cat_id').AsInteger;
//        HitTest := False;
//        Enabled := False;
      end;
    ListBox_categories.AddObject(ListboxGroup);

    // dish create
    FD_dishes.Open();
    FD_dishes.First;
    for var j := 0 to FD_dishes.RecordCount - 1 do begin
      var ListBoxItem := TListBoxItem.Create(Self);
        with ListBoxItem do begin
          Tag := FD_dishes.FieldByName(DISH_ID).AsInteger;
          Text := FD_dishes.FieldByName(DISH_NAME).AsString;
        end;
      ListBox_categories.AddObject(ListboxItem);
      FD_dishes.Next;
    end;

    FD_dish_categories.Next;
  end;

  FD_dish_categories.Close;
  FD_dishes.Close;
  FD_dishes_ALL.Open;
  EndUpdate;
end;

procedure TMain_Form.ListBox_categoriesChange(Sender: TObject);
begin
  var var_dish_id := ListBox_categories.Selected.Tag;
  FD_dishes_ALL.Locate(DISH_ID, var_dish_id, [loCaseInsensitive, loPartialKey]);
  //  show_temp_text(FD_dishes_ALL.FieldByName(DISH_NAME).AsString);
end;

procedure TMain_Form.show_temp_text(text: string);
begin
  temp_text.Text := text;
end;

procedure TMain_Form.test_info_to_dish_map;
begin
  BeginUpdate;
  var products := ['������', '��������', '������ ��.', '�������', '������'];
  var price    := [300.00, 200.00, 200.00, 300.00, 500.00];
  var proteins := [262.0, 21.8, 6.5, 28.0, 25.0];
  var lipids   := [15.3, 14.3, 1.1, 67.0, 5.0];
  var carbs    := [332.9, 113.2, 31.3, 37.0, 63.0];
  var energy   := [3410, 640, 150, 6290, 380];
  dish_map.RowCount := 0;
  dish_map.RowCount := 5;
  for var i := 0 to 4 do begin
    dish_map.Cells[0, i] := (i+1).ToString;
    dish_map.Cells[1, i] := products[i];
    dish_map.Cells[2, i] := price[i].ToString;
    dish_map.Cells[3, i] := proteins[i].ToString;
    dish_map.Cells[4, i] := lipids[i].ToString;
    dish_map.Cells[5, i] := carbs[i].ToString;
    dish_map.Cells[6, i] := energy[i].ToString;
  end;

  EndUpdate;
end;

end.
