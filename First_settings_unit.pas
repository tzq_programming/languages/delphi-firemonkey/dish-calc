unit First_settings_unit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Objects, FMX.Edit;

type
  TForm_first_settings = class(TForm)
    Rectangle_border: TRectangle;
    Layout_workspace: TLayout;
    Label_Hello: TLabel;
    Btn_ready: TSpeedButton;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    Label3: TLabel;
    procedure Btn_readyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_first_settings: TForm_first_settings;

implementation

{$R *.fmx}

uses Main_unit;

procedure TForm_first_settings.Btn_readyClick(Sender: TObject);
begin
  Close;
end;

end.
