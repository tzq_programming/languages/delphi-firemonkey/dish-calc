program Project_menu_calculator;

uses
  System.StartUpCopy,
  FMX.Forms,
  Main_unit in 'Main_unit.pas' {Main_Form},
  First_settings_unit in 'First_settings_unit.pas' {Form_first_settings},
  dish_editor_unit in 'dish_editor_unit.pas' {Dish_Editor_Form};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMain_Form, Main_Form);
  Application.CreateForm(TForm_first_settings, Form_first_settings);
  Application.CreateForm(TDish_Editor_Form, Dish_Editor_Form);
  Application.Run;
end.
