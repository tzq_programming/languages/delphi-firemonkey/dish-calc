unit dish_editor_unit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FMX.Layouts, FMX.ListBox,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Edit, FMX.SearchBox,
  Fmx.Bind.Grid, Data.Bind.Grid;

type
  TDish_Editor_Form = class(TForm)
    GroupBox1: TGroupBox;
    ComboBox_categories: TComboBox;
    Btn_category_add: TSpeedButton;
    Btn_category_edit: TSpeedButton;
    Btn_category_del: TSpeedButton;
    Layout1: TLayout;
    FDcategories: TFDTable;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    GroupBox2: TGroupBox;
    Layout2: TLayout;
    Layout3: TLayout;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    FDdishes: TFDTable;
    ds_cat_dish: TDataSource;
    SG_dishmap: TStringGrid;
    Layout4: TLayout;
    ComboBox_dish_list: TComboBox;
    BindSourceDB2: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    FDProducts: TFDQuery;
    SpeedButton1: TSpeedButton;
    Listbox_product: TListBox;
    SearchBox1: TSearchBox;
    Label1: TLabel;
    BindSourceDB3: TBindSourceDB;
    LinkListControlToField3: TLinkListControlToField;
    Layout5: TLayout;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    SpeedButton4: TSpeedButton;
    Layout6: TLayout;
    Layout7: TLayout;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    Label4: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    Label7: TLabel;
    Edit6: TEdit;
    LinkPropertyToFieldTag: TLinkPropertyToField;
    LinkPropertyToFieldSelectedTag: TLinkPropertyToField;
    FDproduct_list: TFDQuery;
    StringColumn5: TStringColumn;
    Label8: TLabel;
    Edt_full_sum: TEdit;
    procedure Btn_category_delClick(Sender: TObject);
    procedure Btn_category_addClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Btn_category_editClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Listbox_productDblClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SG_dishmapEditingDone(Sender: TObject; const ACol, ARow: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dish_Editor_Form: TDish_Editor_Form;

implementation

{$R *.fmx}

uses Main_unit;

//  if ListBox_categories.ItemIndex < 0 then Exit;
//  if MessageDlg('�� �������, ��� ������ ������� '+ FD_dishes_ALL.FieldByName(DISH_NAME).AsString +'?',
//                TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes then begin
//    FD_dishes_ALL.Delete;
//  end;
//
//  generate_dish_list();

procedure TDish_Editor_Form.Btn_category_addClick(Sender: TObject);
begin
  var var_cat_name := InputBox('����� ���������', '������� �������� ���������', '');
  if var_cat_name = '' then Exit;
  if FDcategories.Locate(CATEGORY_NAME, var_cat_name, [loCaseInsensitive, loPartialKey]) then begin
    ShowMessage('����� ��������� ��� ����������!');
  end
  else begin
    FDcategories.Insert;
    FDcategories.FieldByName(CATEGORY_NAME).Value := var_cat_name;
    FDcategories.Post;
  end;

  FDcategories.Close;
  FDcategories.Open;
  ComboBox_categories.ItemIndex := 0;
end;

procedure TDish_Editor_Form.Btn_category_delClick(Sender: TObject);
begin
  if (ComboBox_categories.ItemIndex < 0) or (ComboBox_categories.Selected.Text = '��� ���������') then Exit;

  if MessageDlg('�� �������, ��� ������ ������� '+ FDcategories.FieldByName(CATEGORY_NAME).AsString +'?',
                TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes then begin
    if MessageDlg('��� �������� ��� ������ ����������� � ��������� "��� ���������". ���������� ��������?',
                TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) = mrYes then FDcategories.Delete;
  end;

end;

procedure TDish_Editor_Form.Btn_category_editClick(Sender: TObject);
begin
  if (ComboBox_categories.ItemIndex < 0) or (ComboBox_categories.Selected.Text = '��� ���������') then Exit;

  var var_old_cat_name := FDcategories.FieldByName(CATEGORY_NAME).AsString;
  var var_cat_name := InputBox('��������� ���������', '������� ����� �������� ���������', FDcategories.FieldByName(CATEGORY_NAME).AsString);

  if var_cat_name = '' then Exit;
  if var_old_cat_name = var_cat_name then Exit;

  var temp_rec_no := FDcategories.RecNo;
  FDcategories.Edit;
  FDcategories.FieldByName(CATEGORY_NAME).Value := var_cat_name;
  FDcategories.Post;

  FDcategories.Close;
  FDcategories.Open;
  FDcategories.RecNo := temp_rec_no;
end;

procedure TDish_Editor_Form.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FDdishes.Close;
  FDcategories.Close;
end;

procedure TDish_Editor_Form.FormShow(Sender: TObject);
begin
  FDcategories.Open;
  FDdishes.Open;
end;

procedure TDish_Editor_Form.Listbox_productDblClick(Sender: TObject);
begin
  var flag := False;
  for var i := 0 to SG_dishmap.RowCount - 1 do begin
    if SG_dishmap.Cells[0, i] = Listbox_product.Selected.Text then begin
      flag := True;
      Break;
    end;
  end;

  if not flag then begin
    SG_dishmap.RowCount := SG_dishmap.RowCount + 1;
    SG_dishmap.Cells[0, SG_dishmap.RowCount - 1] := Listbox_product.Selected.Text;
    SG_dishmap.Cells[2, SG_dishmap.RowCount - 1] := FDproduct_list.FieldByName('prod_price').AsString;
    SG_dishmap.Cells[3, SG_dishmap.RowCount - 1] := FDproduct_list.FieldByName('prod_for_price').AsString;
//    SG_dishmap.Cells[3, SG_dishmap.RowCount - 1] := FDproduct_list.FieldByName('prod_id').AsString;
    SG_dishmap.Cells[SG_dishmap.ColumnCount-1, SG_dishmap.RowCount - 1] := '1';
  end;
end;

procedure TDish_Editor_Form.SG_dishmapEditingDone(Sender: TObject; const ACol,
  ARow: Integer);
begin
  var full_sum := 0.0;
  for var I := 0 to SG_dishmap.RowCount - 1 do begin
    var count_in_dish := StrToFloatDef(SG_dishmap.Cells[1, i], 0);
    var price := StrToFloatDef(SG_dishmap.Cells[2, i], 1);
    var price_for := StrToFloatDef(SG_dishmap.Cells[3, i], 0);
    full_sum := full_sum + count_in_dish / price_for * price;
  end;

  Edt_full_sum.Text := Format('%.2f', [full_sum]);
end;

procedure TDish_Editor_Form.SpeedButton5Click(Sender: TObject);
begin
  if (SG_dishmap.Cells[0, SG_dishmap.RowCount-1] <> '�������� ��������') or (SG_dishmap.RowCount = 0) then begin
    SG_dishmap.RowCount := SG_dishmap.RowCount + 1;
    SG_dishmap.Cells[0, SG_dishmap.RowCount - 1] := '�������� ��������';
    SG_dishmap.Cells[SG_dishmap.ColumnCount-1, SG_dishmap.RowCount - 1] := '0';
  end;

end;

end.
